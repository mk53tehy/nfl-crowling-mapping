
## Installation 
**Voraussetzungen:**  
Um die Funktionalität der Programme zu gewährleisten, müssen folgende Komponenten auf dem Zielsystem erhalten sein:  
- Win10 64bit  
- Python 3.9.1  
- spaCy v2.3.5 (benötigt Microsoft C++ Build Tools)  
- en-core-web-lg v2.3.1 (spaCy Erweiterung)  
- word2number v1.1  
 
**Installation:**  
Falls Python(inkl. Pip) noch nicht auf dem Zielsystem installiert werden sollte, kann sich die benötigte Version auf <https://www.python.org/downloads/> heruntergeladen werden. Diese enthält ebenfalls pip, welches in weiteren Installationsschritten verwendet wird.  
Um spaCy zu installieren, müssen zuerst die setuptools wheel installiert werden. Hierzu muss die Eingabeaufforderung (Konsole) geöffnet werden.  
Anschließend erfolgt dies mit folgendem Befehl:  
<code>pip install -U pip setuptools wheel</code>  

Nun kann spaCy installiert werden. Dies ist mit folgendem Befehl möglich:  
<code>pip install <https://github.com/explosion/spaCy/archive/v2.3.5.zip></code>  
Anmerkung: Sollte die Installation einen Fehler werfen, weist Ihr System nicht die benötigten Installationen der Voraussetzungen für spaCy auf. Diese müssen dann nachinstalliert werden und können in der Regel der Fehlermeldung der spaCy-Installation entnommen werden. Ein häufiger Fehler ist: Microsoft Visual C++ 14.0 or greater is required. Get it with "Microsoft C++ Build Tools", dies ist auf folgender Quelle zu finden: <https://visualstudio.microsoft.com/de/visual-cpp-build-tools>  

Nach erfolgreicher installation von spaCy kann nun die Sprachbibliothek heruntergeladen werden. Dies erfolgt mit folgendem Befehl:  
<code>python -m spacy download en_core_web_lg</code>  

Außerdem wird ein Python-Modul zur Konvertiert von Zahlenwörtern in numerische Repräsentation benötigt. Dies erfolgt mit folgendem Befehl:   
<code>pip install word2number</code> 

Abschließend müssen für Crawling und Scraping noch einige Module, requests und fake-useragent, installiert werden:  
<code>pip install requests fake-useragent</code> 

Nun sind alle Voraussetzungen installiert und das Git Repository kann von folgender Quelle heruntergeladen und auf dem Rechner platziert werden:  
<https://git.informatik.uni-leipzig.de/mk53tehy/nfl-crowling-mapping>

**Ausführung:**  
Navigieren Sie zum Hauptordner des Projektes und anschließend in die Subordner <code>programme</code> und danach <code>get_articles</code>.  
Um das Programm zum Crawling und Scraping zu starten, geben sie bitte folgenden Befehl in die Konsole ein:  
<code>python get_articles.py</code>  
Anmerkungen: Zur Ausführung werden noch andere Dateien benötigt. Diese befinden sich bereits an der korrekten Stelle. Sollten diese nicht auffindbar sein, so wird es Ihnen vom Programm mitgeteilt.
 
Nachdem dieser Schritt erledigt ist, navigieren Sie bitte in den Ordner <code>programme</code> zurück. Hier kann nun das Programm zur für Extraktion und Mapping mit folgendem Befehl ausgeführt werden:  
<code>python extraction.py</code>  
  
Höchstwahrscheinlich kann Durchführung des letzten Schrittes längere Zeit in Anspruch nehmen. Als Ergebnis werden .json-Dateien entstehen, in den sind Ergebnisse des Mappings detailliert zu sehen (7. Ergebnisse des Mappings).
