import os
import glob
import json
import re
from word2number import w2n

import spacy
from numpy import *
from spacy.matcher import Matcher

"""Passing Stats:"""
# Passing Attempts: The number of passes thrown by the player.
# Passing Completions: The number of completions thrown by the player.
# Passing Yards: The number of passing yards thrown by the player.
# Passing Rating: The NFL passer rating for the player in that game.
# Passing Touchdowns: The number of passing touchdowns the player threw.
# Passing Interceptions: The number of interceptions the player threw.
# Passing Sacks: The number of times the player was sacked.
# Passing Sacks Yards Lost: The cumulative yards lost from the player being sacked.

"""Rushing Stats:"""
# Rushing Attempts: The number of times the the player attempted a rush.
# Rushing Yards: The number of yards the player rushed for.
# Rushing Touchdowns: The number of touchdowns the player rushed for.

"""Receiving Stats:"""
# Receiving Targets: The number of times the player was thrown to.
# Receiving Receptions: The number of times the player caught a pass thrown to them.
# Receiving Yards: The number of yards the player gained through receiving.
# Receiving Touchdowns: The number of touchdowns scored through receiving.

"""Kick/Punt Return Stats"""
# Kick Return Attempts: The number of times the player attempted to return a kick.
# Kick Return Yards: The cumulative number of yards the player returned kicks for.
# Kick Return Touchdowns: The number of touchdowns the player scored through kick returns.
# Punt Return Attempts: The number of times the player attempted to return a punt.
# Punt Return Yards: The cumulative number of yards the player returned punts for.
# Punt Return Touchdowns: The number of touchdowns the player scored through punt returns.

"""Kick/Punt Stats"""
# Point After Attempts: The number of PAs the player attempted kicking.
# Point After Makes: The number of PAs the player made.
# Field Goal Attempts: The number of field goals the player attempted.
# Field Goal Makes: The number of field goals the player made.

"""Defense Stats"""
# Sacks: The number of sacks the player got.
# Tackles: The number of tackles the player got.
# Tackle Assists: The number of tackles the player assisted on.
# Interceptions: The number of times the player intercepted the ball.
# Interception Yards: The number of yards the player gained after interceptions.
# Interception Touchdowns: The number of touchdowns the player scored after interceptions.
# Safeties: The number of safeties the player caused.

# Global variables
kaggle_games_file_name = "games_1512362753.8735218.json"  # file name for kaggle games file
kaggle_profiles_file_name = 'profiles.json'  # file name for kaggle profiles file
nlp = spacy.load("en_core_web_lg")  # load large english language pack
text_articles = glob.glob("get_articles/spielartikel/*.txt")  # all fetched article names
global_finds = 0  # number of data that was found
global_equal_finds = 0  # number of data that was equal to kaggle data
global_different_finds = 0  # number of data that was different to kaggle data
global_number_of_articles = 0  # number of articles that was explored
extracted_values = ["passing_attempts",
                    "passing_completions",
                    "passing_yards",
                    "passing_touchdowns",
                    "passing_interceptions",
                    "rushing_attempts",
                    "rushing_yards",
                    "rushing_touchdowns",
                    "receiving_targets",
                    "receiving_receptions",
                    "receiving_yards",
                    "receiving_touchdowns",
                    "field_goal_makes",
                    "defense_tackles"]


def main():
    """
    Extraction and mapping a data from articles
    """
    for article in text_articles:
        article_name = article
        article = open(article_name).read().replace('\n', '')
        article = re.sub("'s", '', str(article))  # removing apostrophes with s
        article = re.sub("'", '', str(article))  # removing apostrophes
        doc = nlp(article)
        article_name_data = article_name.split("\\")[1].split("__")
        year = article_name_data[0]
        team = article_name_data[1]
        opponent = article_name_data[2]
        team_score = article_name_data[3]
        opponent_score = article_name_data[4].split(".")[0]
        data_article = {"id": year + "__" + team + "__" + opponent + "__" + team_score + "__" + opponent_score,
                        "content": article}

        # Find kaggle data for article
        kaggle_games_file_for_current_year = open("kaggle_data/games/" + year + ".json")
        kaggle_games_data_for_current_year = json.load(kaggle_games_file_for_current_year)
        kaggle_games_for_current_article = []
        for game in kaggle_games_data_for_current_year:
            if (game["team"] == team and game["opponent"] == opponent and game["player_team_score"] == team_score and
                game["opponent_score"] == opponent_score) or (game["team"] == opponent and game["opponent"] == team and
                                                              game["player_team_score"] == opponent_score and
                                                              game["opponent_score"] == team_score):
                kaggle_games_for_current_article.append(game)

        # Process an article only if a kaggle data exists
        if len(kaggle_games_for_current_article) > 0:
            global global_number_of_articles
            global_number_of_articles += 1

            data_article['sentences'] = [str(sentence) for sentence in list(doc.sents)]  # split article into sentences
            people = [str(ent) for ent in doc.ents if ent.label_ == "PERSON"]  # find names in article
            people = list(dict.fromkeys(people))  # delete name duplicates

            # open all player profiles from kaggle
            kaggle_player_ids_for_article = [game["player_id"] for game in kaggle_games_for_current_article]
            kaggle_profiles_file = open(kaggle_profiles_file_name)
            kaggle_profiles_data = json.load(kaggle_profiles_file)

            # Find NFL players in people from article
            kaggle_players_in_article = []
            for person in people:
                for profile in kaggle_profiles_data:
                    # Delete leading and trailing spaces in kaggle player name, lowercase both, check if kaggle player
                    # id is in kaggle game
                    if str(person).lower() == str(profile["name"].strip()).lower():
                        if len(kaggle_player_ids_for_article) != 0 and profile["player_id"] in kaggle_player_ids_for_article:
                            person = {"player_id": str(profile["player_id"]), "name": person}
                            kaggle_players_in_article.append(person)

            # Create patterns
            pattern_name = [{'ENT_TYPE': 'PERSON', "OP": "+"}]
            pattern_name2 = [{'ENT_TYPE': 'ORG'}]
            pattern_passing_attempts_and_passing_completions = [{"TEXT": {"REGEX": "(-of-)"}}]
            pattern_passing_yards = [{"TEXT": {"REGEX": "(-of-)"}}, {"OP": "+"}, {"LIKE_NUM": True}, {"LEMMA": "yard"}]
            pattern_passing_touchdown = [{'POS': 'DET', 'OP': '+'}, {'POS': 'ADJ', 'OP': '?'},
                                            {'LEMMA': 'touchdown'}]
            pattern_passing_touchdowns = [{'LIKE_NUM': True, 'OP': '+'}, {'OP': '?'}, {'LEMMA': 'touchdown'}]
            pattern_passing_interception = [{'POS': 'DET', 'OP': '+'}, {'POS': 'ADJ', 'OP': '?'},
                                               {'LEMMA': 'interception'}]
            pattern_passing_interceptions = [{'LIKE_NUM': True, 'OP': '+'}, {'POS': 'ADJ', 'OP': '?'},
                                             {'LEMMA': 'interception'}]
            pattern_defense_tackles = [{'LIKE_NUM': True}, {"ORTH": "tackles"}]
            pattern_receiving_receptions_yards_and_touchdowns = [{'ORTH': '('}, {'LIKE_NUM': True}, {'ORTH': '-'},
                                                                 {'LIKE_NUM': True}, {'IS_PUNCT': True, 'OP': '?'},
                                                                 {'OP': '?'}, {'ORTH': ')'}]
            pattern_receiving_targets_receptions_yards_and_touchdowns = [{'LIKE_NUM': True}, {'ORTH': 'of'},
                                                                         {'ORTH': 'his', 'OP': '?'}, {'LIKE_NUM': True},
                                                                         {'LEMMA': 'target'}, {'ORTH': 'for'},
                                                                         {'LIKE_NUM': True}, {'LEMMA': 'yard'}]
            pattern_receiving_receptions_and_receiving_yards = [{'LIKE_NUM': True}, {'LEMMA': 'catch', "OP": "?"},
                                                                {'LEMMA': 'grab', "OP": "?"},
                                                                {'LEMMA': 'ball', "OP": "?"},
                                                                {'ORTH': 'for'}, {'LIKE_NUM': True},
                                                                {"ORTH": "receiving", "OP": "?"}, {'LEMMA': 'yard'}]
            pattern_receiving_yards = [{"LIKE_NUM": True}, {"ORTH": "receiving"}, {"LEMMA": "yard"}]
            pattern_rushing_attempts_and_rushing_yards = [{'LIKE_NUM': True}, {"ORTH": "rushing", "OP": "?"},
                                                          {'LEMMA': 'yard'}, {'ORTH': 'on'}, {'LIKE_NUM': True},
                                                          {'LEMMA': 'carry', "OP": "?"},
                                                          {'LEMMA': 'attempt', "OP": "?"},
                                                          {'LEMMA': 'scramble', "OP": "?"}]
            pattern_rushing_yards = [{"LIKE_NUM": True}, {"ORTH": "rushing"}, {"LEMMA": "yard"}]
            pattern_field_goal_makes = [{'ORTH': 'field'}, {'ORTH': 'goal'}, {'ORTH': 'from'}, {'ENT_TYPE': 'PERSON'},
                                        {'ENT_TYPE': 'PERSON'}]
            pattern_passing_and_rushing_touchdowns = [{'ORTH': "("}, {"LIKE_NUM": True}, {'ORTH': "passing"},
                                                      {"ORTH": ","}, {"LIKE_NUM": True}, {'ORTH': "rushing"},
                                                      {'ORTH': ")"}]

            # Create matcher
            matcher = Matcher(nlp.vocab)

            # Add patterns to matcher
            matcher.add("names", None, pattern_name, pattern_name2)
            matcher.add("passing_attempts_and_passing_completions", None,
                        pattern_passing_attempts_and_passing_completions)
            matcher.add("passing_yards", None, pattern_passing_yards)
            matcher.add("passing_touchdowns", None, pattern_passing_touchdowns, pattern_passing_touchdown)
            matcher.add("passing_interceptions", None, pattern_passing_interceptions, pattern_passing_interception)
            matcher.add("defense_tackles", None, pattern_defense_tackles)
            matcher.add("receiving_receptions_yards_and_touchdowns", None,
                        pattern_receiving_receptions_yards_and_touchdowns,
                        pattern_receiving_receptions_and_receiving_yards)
            matcher.add("receiving_yards", None, pattern_receiving_yards)
            matcher.add("receiving_targets_receptions_yards_and_touchdowns", None,
                        pattern_receiving_targets_receptions_yards_and_touchdowns)
            matcher.add("rushing_attempts_and_rushing_yards", None, pattern_rushing_attempts_and_rushing_yards)
            matcher.add("rushing_yards", None, pattern_rushing_yards)
            matcher.add("field_goal_makes", None, pattern_field_goal_makes)
            matcher.add("passing_and_rushing_touchdowns", None, pattern_passing_and_rushing_touchdowns)

            data_article["sentences_stats"] = []
            stat_sent_id = 0
            for sent in list(doc.sents):
                matches = matcher(sent)
                sentence_stats = {
                    "id": stat_sent_id,
                    "sentence": str(sent),
                    "players": [],
                    "stats": {"defense_tackles": [],
                              "passing_attempts_passing_completions": [],
                              "passing_yards": [],
                              "passing_touchdowns": [],
                              "passing_interceptions": [],
                              "receiving_receptions_receiving_yards_receiving_touchdowns": [],
                              "receiving_targets_receiving_receptions_receiving_yards_receiving_touchdowns": [],
                              "receiving_yards": [],
                              "rushing_attempts_rushing_yards": [],
                              "rushing_touchdowns": [],
                              "rushing_yards": [],
                              "passing_touchdowns_rushing_touchdowns": [],
                              "field_goal_makes": []}}
                if len(matches) > 0:
                    for match_id, start, end in matches:
                        span = sent[start:end]  # the matched span
                        string_id = nlp.vocab.strings[match_id]  # get name of pattern

                        # Get first and second left token
                        token_array = []
                        for i in range(1, 3):
                            try:
                                token_array.append(str(sent[start - i]).lower())
                            except IndexError:
                                token_array.append('NOT EXISTS')

                        # Check the matches for all creating patterns
                        if string_id == "names":
                            name = str(span.text).lower()
                            for player in kaggle_players_in_article:
                                if name in str(player["name"]).lower():
                                    if check_number_of_identical_names(name, kaggle_players_in_article):
                                        player_in_sent = player.copy()
                                        break
                            else:
                                player_in_sent = None
                            if player_in_sent is not None and player_in_sent["player_id"] not in [player["player_id"] for player in sentence_stats["players"]]:
                                sentence_stats["players"].append(player_in_sent)
                        elif string_id == "passing_attempts_and_passing_completions":
                            passing_attempts = span.text.split("-of-")[0]
                            passing_completions = span.text.split("-of-")[1]
                            order_new_stats(token_array,
                                            {"passing_attempts": passing_attempts,
                                             "passing_completions": passing_completions},
                                            sentence_stats,
                                            kaggle_players_in_article)
                        elif string_id == "passing_yards":
                            passing_yards = str(sent[end - 2])
                            order_new_stats(token_array,
                                            {"passing_yards": passing_yards},
                                            sentence_stats,
                                            kaggle_players_in_article)
                        elif string_id == "receiving_receptions_yards_and_touchdowns":
                            if str(span[0]) == "(":
                                receiving_receptions = str(span[1])
                            else:
                                receiving_receptions = str(span[0])
                            receiving_yards = str(span[3])
                            receiving_touchdowns = None
                            try:
                                if str(span[5]) == "TD":
                                    receiving_touchdowns = "1"
                                else:
                                    try:
                                        receiving_touchdowns = str(int(str(span[5])))
                                    except ValueError:
                                        pass
                            except IndexError:
                                pass
                            order_new_stats(token_array,
                                            {"receiving_receptions": receiving_receptions,
                                             "receiving_yards": receiving_yards,
                                             "receiving_touchdowns": receiving_touchdowns},
                                            sentence_stats,
                                            kaggle_players_in_article)
                        elif string_id == "receiving_targets_receptions_yards_and_touchdowns":
                            receiving_receptions = str(span[0])
                            receiving_targets = str(span[2])
                            receiving_yards = str(span[5])
                            receiving_touchdowns = None
                            if "his" in [str(item) for item in span]:
                                receiving_targets = str(span[3])
                                receiving_yards = str(span[6])
                            if "and" in str(sent[end]):
                                if "a" in str(sent[end + 1]) or "the" in str(sent[end + 1]):
                                    receiving_touchdowns = 1
                                else:
                                    try:
                                        receiving_touchdowns = w2n.word_to_num(str(sent[end + 1]))
                                    except ValueError:
                                        pass
                            order_new_stats(token_array,
                                            {"receiving_targets": receiving_targets,
                                             "receiving_receptions": receiving_receptions,
                                             "receiving_yards": receiving_yards,
                                             "receiving_touchdowns": receiving_touchdowns},
                                            sentence_stats,
                                            kaggle_players_in_article)
                        elif string_id == "receiving_yards":
                            receiving_yards = str(span[0])
                            order_new_stats(token_array,
                                            {"receiving_yards": receiving_yards},
                                            sentence_stats,
                                            kaggle_players_in_article)
                        elif string_id == "rushing_attempts_and_rushing_yards":
                            rushing_yards = str(span[0])
                            rushing_attempts = str(span[3])
                            if "rushing" in [str(item) for item in span]:
                                rushing_attempts = str(span[4])
                            order_new_stats(token_array,
                                            {"rushing_attempts": rushing_attempts,
                                             "rushing_yards": rushing_yards},
                                            sentence_stats,
                                            kaggle_players_in_article)
                        elif string_id == "rushing_yards":
                            rushing_yards = str(span[0])
                            order_new_stats(token_array,
                                            {"rushing_yards": rushing_yards},
                                            sentence_stats,
                                            kaggle_players_in_article)
                        elif string_id == "field_goal_makes":
                            field_goal_makes = "1"
                            order_new_stats(token_array,
                                            {"field_goal_makes": field_goal_makes},
                                            sentence_stats,
                                            kaggle_players_in_article)
                        elif string_id == "passing_touchdowns":
                            if len(sentence_stats["stats"]["passing_attempts_passing_completions"]) > 0 and len(sent) > end + 1 and "(" not in str(sent[end + 1]):
                                passing_touchdowns = "1"
                                try:
                                    passing_touchdowns = str(w2n.word_to_num(str(span[0])))
                                except ValueError:
                                    pass
                                order_new_stats(token_array,
                                                {"passing_touchdowns": passing_touchdowns},
                                                sentence_stats,
                                                kaggle_players_in_article)
                        elif string_id == "passing_interceptions":
                            if len(sentence_stats["stats"]["passing_attempts_passing_completions"]) > 0:
                                passing_interceptions = "1"
                                try:
                                    passing_interceptions = str(w2n.word_to_num(str(span[0])))
                                except ValueError:
                                    pass

                                order_new_stats(token_array,
                                                {"passing_interceptions": passing_interceptions},
                                                sentence_stats,
                                                kaggle_players_in_article)
                        elif string_id == "passing_and_rushing_touchdowns":
                            passing_touchdowns = str(span[1])
                            rushing_touchdowns = str(span[4])
                            order_new_stats(token_array,
                                            {"passing_touchdowns": passing_touchdowns,
                                            "rushing_touchdowns": rushing_touchdowns},
                                            sentence_stats,
                                            kaggle_players_in_article)
                        elif string_id == "defense_tackles":
                            defense_tackles = str(span[0])
                            order_new_stats(token_array,
                                            {"defense_tackles": defense_tackles},
                                            sentence_stats,
                                            kaggle_players_in_article)
                order_stats_to_founded_player_in_sentence(sentence_stats)
                data_article["sentences_stats"].append(sentence_stats)
                if stat_sent_id > 1:
                    find_players_in_previous_sentences_and_order_stats(data_article["sentences_stats"])
                stat_sent_id += 1
            group_stats(data_article)
            words_to_numbers(data_article["games"])
            compare_data(data_article["games"], kaggle_games_for_current_article)
            print(global_finds, global_equal_finds, global_different_finds, data_article["id"])
            write_article_data_to_file(data_article)


def order_new_stats(token_array, stats, sentence_stats, kaggle_players_in_article):
    """
    Order new founded stats in a sentence
    @param token_array:
    @param stats: new stats in sentence
    @param sentence_stats: all stats in sentence
    @param kaggle_players_in_article: kaggle players in current article
    """
    for player in sentence_stats["players"]:
        if is_token_in_article_players_and_not_duplicated_in_sentence(token_array, player["name"], kaggle_players_in_article):
            for k, v in stats.items():
                if v not in player.keys() and v is not None:
                    player[k] = v
            break
    else:
        key = None
        value = None
        if len(stats) == 1:
            key = list(stats.keys())[0]
            value = list(stats.values())[0]
        elif len(stats) > 1:
            key = "_".join(stats.keys())
            value = "-".join([str(value) for value in stats.values() if value is not None])

        if value not in sentence_stats["stats"][key]:
            sentence_stats["stats"][key].append(value)


def is_token_in_article_players_and_not_duplicated_in_sentence(token_array, player, players):
    """
    Check if tokens in the left(it can be first name or last name) equal
    to player name and this player not in stats for this sentence
    @param token_array: two left tokens from data
    @param player: player, that was found
    @param players: array with players in sentence
    @return: true if one of three tokens in player name and no other players
             with this name in sentence stats, otherwise false
    """
    player = str(player).lower()
    for token in token_array:
        if token in player and check_number_of_identical_names(token, players):
            return True
    return False


def order_stats(player, sentence_stats):
    """
    Order all unordered stats to given player
    @param player: player to order
    @param sentence_stats: sentence stats
    """
    if len(sentence_stats["stats"]["passing_attempts_passing_completions"]) == 1:
        if "passing_attempts" not in player.keys():
            player["passing_attempts"] = sentence_stats["stats"]["passing_attempts_passing_completions"][0].split("-")[0]
        if "passing_completions" not in player.keys():
            player["passing_completions"] = sentence_stats["stats"]["passing_attempts_passing_completions"][0].split("-")[1]
    if len(sentence_stats["stats"]["passing_yards"]) == 1:
        if "passing_yards" not in player.keys():
            player["passing_yards"] = sentence_stats["stats"]["passing_yards"][0]
    if len(sentence_stats["stats"]["passing_touchdowns"]) == 1:
        if "passing_touchdowns" not in player.keys():
            player["passing_touchdowns"] = sentence_stats["stats"]["passing_touchdowns"][0]
    if len(sentence_stats["stats"]["passing_interceptions"]) == 1:
        if "passing_interceptions" not in player.keys():
            player["passing_interceptions"] = sentence_stats["stats"]["passing_interceptions"][0]
    if len(sentence_stats["stats"]["rushing_attempts_rushing_yards"]) == 1:
        if "rushing_attempts" not in player.keys():
            player["rushing_attempts"] = sentence_stats["stats"]["rushing_attempts_rushing_yards"][0].split("-")[0]
        if "rushing_yards" not in player.keys():
            player["rushing_yards"] = sentence_stats["stats"]["rushing_attempts_rushing_yards"][0].split("-")[1]
    if len(sentence_stats["stats"]["rushing_yards"]) == 1:
        if "rushing_yards" not in player.keys():
            player["rushing_yards"] = sentence_stats["stats"]["rushing_yards"][0]
    if len(sentence_stats["stats"]["receiving_receptions_receiving_yards_receiving_touchdowns"]) == 1:
        if "receiving_receptions" not in player.keys():
            player["receiving_receptions"] = sentence_stats["stats"]["receiving_receptions_receiving_yards_receiving_touchdowns"][0].split("-")[0]
        if "receiving_yards" not in player.keys():
            player["receiving_yards"] = sentence_stats["stats"]["receiving_receptions_receiving_yards_receiving_touchdowns"][0].split("-")[1]
        if len(sentence_stats["stats"]["receiving_receptions_receiving_yards_receiving_touchdowns"][0].split("-")) == 3:
            if "receiving_touchdowns" not in player.keys():
                player["receiving_touchdowns"] = sentence_stats["stats"]["receiving_receptions_receiving_yards_receiving_touchdowns"][0].split("-")[2]
    if len(sentence_stats["stats"]["receiving_targets_receiving_receptions_receiving_yards_receiving_touchdowns"]) == 1:
        if "receiving_targets" not in player.keys():
            player["receiving_targets"] = sentence_stats["stats"]["receiving_targets_receiving_receptions_receiving_yards_receiving_touchdowns"][0].split("-")[0]
        if "receiving_receptions" not in player.keys():
            player["receiving_receptions"] = sentence_stats["stats"]["receiving_targets_receiving_receptions_receiving_yards_receiving_touchdowns"][0].split("-")[1]
        if "receiving_yards" not in player.keys():
            player["receiving_yards"] = sentence_stats["stats"]["receiving_targets_receiving_receptions_receiving_yards_receiving_touchdowns"][0].split("-")[2]
        if len(sentence_stats["stats"]["receiving_targets_receiving_receptions_receiving_yards_receiving_touchdowns"][0].split("-")) == 4:
            if "receiving_touchdowns" not in player.keys():
                player["receiving_touchdowns"] = sentence_stats["stats"]["receiving_targets_receiving_receptions_receiving_yards_receiving_touchdowns"][0].split("-")[3]
    if len(sentence_stats["stats"]["field_goal_makes"]) == 1 and "field_goal_makes" not in player.keys():
        player["field_goal_makes"] = sentence_stats["stats"]["field_goal_makes"][0]
    if len(sentence_stats["stats"]["passing_touchdowns_rushing_touchdowns"]) == 1:
        if "passing_touchdowns" not in player.keys():
            player["passing_touchdowns"] = sentence_stats["stats"]["passing_touchdowns_rushing_touchdowns"][0].split("-")[0]
        if "rushing_touchdowns" not in player.keys():
            player["rushing_touchdowns"] = sentence_stats["stats"]["passing_touchdowns_rushing_touchdowns"][0].split("-")[1]
    if len(sentence_stats["stats"]["defense_tackles"]) == 1 and "defense_tackles" not in player.keys():
        player["defense_tackles"] = sentence_stats["stats"]["defense_tackles"][0]


def order_stats_to_founded_player_in_sentence(sentence_stats):
    """
    Order the founded stats to an player, that present in sentence
    @param sentence_stats: stats from current sentence
    """
    if len(sentence_stats["players"]) == 1:
        player = sentence_stats["players"][0]
        order_stats(player, sentence_stats)
    elif len(sentence_stats["players"]) > 1:
        player = sentence_stats["players"][len(sentence_stats["players"]) - 1]
        order_stats(player, sentence_stats)


def find_players_in_previous_sentences_and_order_stats(sentence_stats):
    """
    If in the sentence no players are present and some stats given, order these
    to the last player from previous sentences(max. until 2)
    @param sentence_stats: stats for the current sentence
    """
    length = len(sentence_stats)
    current_sentence_stats = sentence_stats[length - 1]
    last_player = None
    if len(current_sentence_stats["players"]) == 0:
        previous_sentence_stats = sentence_stats[length - 2]
        before_previous_sentence_stats = sentence_stats[length - 3]
        if len(previous_sentence_stats["players"]) > 0:
            last_player = previous_sentence_stats["players"][len(previous_sentence_stats["players"]) - 1]
        elif len(before_previous_sentence_stats["players"]) > 0:
            last_player = before_previous_sentence_stats["players"][len(before_previous_sentence_stats["players"]) - 1]
        if last_player is not None:
            order_stats(last_player, current_sentence_stats)


def words_to_numbers(games):
    """
    Convert all number words into integers
    @param games: stats
    """
    for game in games:
        for key in game.keys():
            if key in extracted_values:
                try:
                    game[key] = w2n.word_to_num(game[key])
                except ValueError:
                    pass


def group_stats(data_article):
    """
    Group all stats from all sentences for each player and save it into data_article
    @param data_article: data stats from article
    """
    games = []
    for sentence_stats in data_article["sentences_stats"]:
        if len(sentence_stats["players"]) > 0:
            for player in sentence_stats["players"]:
                if len(games) == 0:
                    games.append(player.copy())
                    continue
                else:
                    for game_item in games:
                        if player["player_id"] == game_item["player_id"]:
                            for key in player.keys():
                                if key not in game_item.keys():
                                    game_item[key] = player[key]
                            break
                    else:
                        games.append(player.copy())
    data_article["games"] = games.copy()


def compare_data(article_data, kaggle_data):
    """
    Comparing the data from article and kaggle data, found differences and save it
    @param article_data: stats data from article
    @param kaggle_data:  stats data from kaggle
    """
    global global_finds, global_equal_finds, global_different_finds

    for item_article in article_data:
        for item_kaggle in kaggle_data:
            if int(item_article["player_id"]) == item_kaggle["player_id"]:
                difference_array = []
                for k, v in item_article.items():
                    if k in item_kaggle and k in extracted_values:
                        # if item_kaggle[k] != 0:  # check if the value in kaggle = 0
                        global_finds = global_finds + 1
                        if item_kaggle[k] != v:
                            difference = {k: {"article": v, "kaggle": item_kaggle[k]}}
                            difference_array.append(difference)
                            global_different_finds = global_different_finds + 1
                        else:
                            global_equal_finds = global_equal_finds + 1
                if len(difference_array) > 0:
                    item_article["difference"] = difference_array


def write_article_data_to_file(data_article):
    """
    Write an article data to json file
    @param data_article: article json
    """
    if not os.path.exists("data"):
        os.makedirs("data")
    data_raw_file = open("data/" + data_article['id'] + ".json", 'w')
    json.dump(data_article, data_raw_file, indent=4, sort_keys=False)  # write extracted data to file
    data_raw_file.close()


def split_kaggle_data():
    """
    Split kaggle games data by years
    """
    if not os.path.exists("kaggle_data/games"):
        print("Split kaggle data by years...")
        os.makedirs("kaggle_data/games")
        start_year = 1990
        end_year = 2017
        file = open(kaggle_games_file_name)
        file_data = json.load(file)

        for i in range(start_year, end_year + 1):
            games_in_this_year = []
            for game in file_data:
                if int(game["year"]) == i:
                    games_in_this_year.append(game)

            game_file_name = str(i) + ".json"
            out_file = open("kaggle_data/games/" + game_file_name, "w")
            json.dump(games_in_this_year, out_file, indent=4, sort_keys=False)
            out_file.close()
    else:
        print("Directory for kaggle games data is already created and data is splitted")


def is_file_exists(name):
    """
    Check if file exists
    :param name: file name
    :return: True if exists, otherwise false
    """
    try:
        file = open(name)
        file.close()
        print("File " + name + " exists")
        return True
    except FileNotFoundError:
        print("Error: file " + name + " in root folder not found!")
        return False


def check_number_of_identical_names(token, players):
    """
    Check if the number of token occurrences in array of kaggle players in article bigger then 1
    :param token: possible player name
    :param players: kaggle players in article
    :return: True if 1 or 0, false bigger then 1
    """
    i = 0
    for player in players:
        if token in str(player["name"]).lower():
            i += 1
    if i > 1:
        return False
    else:
        return True


def print_statistic():
    """
    Print overall stats
    """
    if global_number_of_articles != 0:
        print("Number of articles: " + str(global_number_of_articles))
        if global_finds != 0:
            print("Finds per article: " + str("{:.2f}".format(float(global_finds / global_number_of_articles))))
            print("All finds: " + str(global_finds))
            print("Equal finds: " + str(global_equal_finds), str("{:.2f}".format(float((global_equal_finds / global_finds) * 100))) + "%")
            print("Different finds: " + str(global_different_finds),
                  str("{:.2f}".format(float((global_different_finds / global_finds) * 100))) + "%")
    else:
        print("Error: no articles found")


def init():
    """
    Start function
    """
    if is_file_exists(kaggle_games_file_name) and is_file_exists(kaggle_profiles_file_name):
        split_kaggle_data()
        main()
        print_statistic()
    else:
        print("Program ends with error!")


init()
