import re
import requests
from fake_useragent import UserAgent
import io
import os
import sys

def cleanhtml(raw_html):
	"""
	function to remove html code and it's content
	@param raw_html: input string
	@return: returns the result (input string without html)
	"""
	cleanr = re.compile('<.*?>')
	cleantext = re.sub(cleanr, '', raw_html)
	return cleantext

def initialize_directory():
	"""
	checks if working directories exist and creates them, if necessary
	"""
	try:
		os.makedirs("./wochenartikel")
		print("creating directory: ./wochenartikel...")
	except FileExistsError:
		print("Note: directory ./wochenartikel already exists, skipping...")
		pass
	
	try:
		os.makedirs("./spielartikel")
		print("creating directory: ./spielartikel...")
	except FileExistsError:
		print("Note: directory ./spielartikel already exists, skipping...")
		pass

def initialize_necessary_files():
	"""
	checks if necessesary files (team_abbreviations.txt) are present
	if that's not the case, the program will quit itself because those files are mandatory
	@return: returns the adjusted input value(1)
	"""
	try:
		test_file = open("team_abbreviations.txt")
		test_file.close()
		team_abbreviations_exist = 1
		return team_abbreviations_exist
	except IOError:
		print("Error: team_abbreviations.txt not found in ./team_abbreviations.txt")
		print("Error: please make sure team_abbreviations.txt is located in the correct directory")
		print("Quitting...")
		sys.exit(0)
	except UnboundLocalError:
		print("Error: team_abbreviations.txt not found in ./team_abbreviations.txt")
		print("Error: please make sure team_abbreviations.txt is located in the correct directory")
		print("Quitting...")
		sys.exit(0)
	except:
		print("Error: unkwown error in initialize_necessary_files")
		print("Quitting...")
		sys.exit(0)

def download():
	"""
	downloads the source-php from the website using a fake user agent to bypass 403 errors
	"""
	#example: https://walterfootball.com/nflreview2014_17.php
	for i in range(len(url_week)):
		filepath = "./wochenartikel/" + url_year + "__" + url_week[i] + ".php"
		print("downloading " + filepath + "...")
		ua_str = UserAgent().chrome
		url = url_base + url_year + url_mid + url_week[i] + url_end
		result = requests.get(url, headers={"User-Agent": ua_str})
		website_file = open(filepath, 'wb')
		website_file.write(result.content)
		website_file.close()

def prepare_article_collection():
	"""
	prepares the downloaded articles by using each weeks downloaded php file
	by extracting the articles
	removing the php coding (aswell as dump lines)
	and adding seperator lines in between the seperate articles
	"""
	for x in range(len(url_week)):
		#empty line list in case it's filled
		for i in range(len(lines)):
			lines.pop(0)
		filepath = "./wochenartikel/" + url_year + "__" + url_week[x] + ".php"
		website_file = io.open(filepath, "r", encoding="utf-8")
		for line in website_file:
			if re.search("[a-zA-Z0-9]", line) != None:
				lines.append(line)
		website_file.close()
		
		for i in range(len(lines)):
			if lines[i].startswith("Editor's Note"):
				lines[i] = "THIS_IS_A_LINE_TO_BE_DELETED\n"
				#note: 1 line above is 2 or 3 lines sometimes due to format of the .php
				for z in range(3):
					if lines[i-z].startswith("By "):
						lines[i-z] = "THIS_IS_A_LINE_TO_BE_DELETED\n"
		
		filepath = filepath.replace(".php", ".txt")
		print("writing " + filepath + "...")
		website_file = io.open(filepath, "w", encoding="utf-8")
		control = 0
	
		for i in range(len(lines)):
			if control == 1:
				end = """<font size = 3>"""
				sep = """ <img src="/images/fball/jetsb_logo.gif">   <img src="/images/fball/dolphinsb_logo.gif">"""
				if re.findall(r'<font size', lines[i]) == re.findall(r'<font size', end):
					control = 2
				elif re.findall(r'img src=', lines[i]) == re.findall(r'img src=', sep):
					lines[i] = "THIS_IS_A_SEPARATOR_LINE\n"
					website_file.write(cleanhtml(lines[i]))
				elif re.findall(r'THIS_IS_A_LINE_TO_BE_DELETED', lines[i]) == re.findall(r'THIS_IS_A_LINE_TO_BE_DELETED', "THIS_IS_A_LINE_TO_BE_DELETED"):
					control = control
				else:
					lines[i] = re.sub(r'^[^a-zA-z0-9]*', '', cleanhtml(lines[i]))
					website_file.write(lines[i])
			if control == 0:
				start = """<div class='content-block' id="MainContentBlock">"""
				if re.findall(r'id="(.*?)">', lines[i]) == re.findall(r'id="(.*?)">', start):
					control = 1
		website_file.close()

def get_single_articles():
	"""
	splits the games articles in each source file
	and writes each of them into a .txt file
	while naming them [year]__[team-1]__[team-2]__[points-team-1]__[points-team-2].txt
	"""
	for x in range(len(url_week)):
		filepath_source = "./wochenartikel/" + url_year + "__" + url_week[x] + ".txt"
		for i in range(len(lines)):
			lines.pop(0)
		article_counter = 0
		source_file = io.open(filepath_source, "r", encoding="utf-8")
		for line in source_file:
			if re.findall(r'THIS_IS_A_SEPARATOR_LINE', line) == re.findall(r'THIS_IS_A_SEPARATOR_LINE', "THIS_IS_A_SEPARATOR_LINE"):
				if len(lines) >= 1:
					filepath_target = "./spielartikel/" + url_year + "__" + str(article_counter) + ".txt"
					if re.findall(r'[a-zA-Z0-9]+ [0-9]+, [ a-zA-Z0-9]+ [0-9]+', lines[0]) != None:
						temp = re.findall(r'[a-zA-Z0-9]+ [0-9]+', lines[0])
						name_1 = re.findall(r'[a-zA-Z0-9]+ ', temp[0])[0].replace(" ", "__")
						name_2 = re.findall(r'[a-zA-Z0-9]+ ', temp[1])[0].replace(" ", "__")
						
						#avoiding conflicts by renaming a specific team name(when present), which isn't required at this point anymore
						temp[0] = temp[0].replace("49ers", "FOURTY_NINE_REPLACEMENT")
						temp[1] = temp[1].replace("49ers", "FOURTY_NINE_REPLACEMENT")
						
						points_1 = re.findall(r'[0-9]+', temp[0])[0] + "__"
						points_2 = re.findall(r'[0-9]+', temp[1])[0]
						if len(name_1) <= 0:
							name_1 = "ERROR_UNKNOWN_TEAM"
						if len(name_2) <= 0:
							name_2 = "ERROR_UNKNOWN_TEAM"
						if len(points_1) <= 0:
							points_1 = "ERROR_UNKNOWN_POINTS"
						if len(points_2) <= 0:
							points_2 = "ERROR_UNKNOWN_POINTS"
						if team_abbreviations_exist == 1:
							if name_1 != "ERROR_UNKNOWN_TEAM":
								name_1 = name_1.replace("__", "")
								name_1 = identify_team(name_1)
								name_1 = name_1 + "__"
							if name_2 != "ERROR_UNKNOWN_TEAM":
								name_2 = name_2.replace("__", "")
								name_2 = identify_team(name_2)
								name_2 = name_2 + "__"
						filepath_target = "./spielartikel/" + url_year + "__" + name_1 + name_2 + points_1 + points_2 + ".txt"
					target_file = io.open(filepath_target, "w", encoding="utf-8")
					print("writing " + filepath_target + "...")
					for i in range(len(lines)):
						if i != 0:
							target_file.write(lines[i])
					target_file.close()
				for i in range(len(lines)):
					lines.pop(0)
				article_counter = article_counter + 1
			else:
				lines.append(line)
		source_file.close()

def identify_team(team_name_article_format):
	"""
	attempts to identify the actual team associated with the team named in the article
	if positive, the team name will be replaced by it's abbreviation
	if negative, won't change anything and will print a warning
	"""
	team_name_file = io.open("./team_abbreviations.txt", "r", encoding="utf-8")
	team_identified = 0
	for line in team_name_file:
		if re.findall(r'^[A-Z]+[ ]+[A-Z][ a-zA-Z0-9]+[a-zA-Z0-9]', line) != None:
			team_name_abb = re.findall(r'^[A-Z]+', line)
			team_name_long = re.findall(r'[A-Z][a-z][ a-zA-Z0-9]+[a-zA-Z0-9]', line)
			if team_name_article_format in team_name_long[0]:
				team_name_article_format = team_name_abb[0]
				team_identified = 1
	team_name_file.close()
	if team_identified == 0:
		print("Warning: team '" + team_name_article_format + "' was not identified, using base name instead!!!")
	return team_name_article_format

#define basic variables
url_base = "https://walterfootball.com/nflreview"
url_mid = "_"
url_end = ".php"
url_year = "2014"
url_week = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17"]
lines = []
article_counter = 0
team_abbreviations_exist = 0

initialize_directory()
team_abbreviations_exist = initialize_necessary_files()
download()
prepare_article_collection()
get_single_articles()
